SELECT 

[ActivityId],
coalesce([ActualStart],[ActualEND] )AS ActivityDate,
[dbo].[StripHTML]([Description]) [DescriptionUpdated],
[Description],
[OwnerId],
----consider collation difference
(select top 1 sf.id from [dbo].[TempSFOwner] sf inner join [ExecutiveEd_MSCRM].dbo.owner c on  sf.name COLLATE SQL_Latin1_General_CP1_CI_AS = c.Name COLLATE SQL_Latin1_General_CP1_CI_AS
and c.OwnerId= ap.[OwnerId]) SFUserID, 

(select top 1 sf.Name from [dbo].[TempSFOwner] sf inner join [ExecutiveEd_MSCRM].dbo.owner c on  sf.name COLLATE SQL_Latin1_General_CP1_CI_AS = c.Name COLLATE SQL_Latin1_General_CP1_CI_AS
and c.OwnerId= ap.[OwnerId]) SFUserName, 

(select top 1 sf.IsActive from [dbo].[TempSFOwner] sf inner join [ExecutiveEd_MSCRM].dbo.owner c on  sf.name COLLATE SQL_Latin1_General_CP1_CI_AS = c.Name COLLATE SQL_Latin1_General_CP1_CI_AS
and c.OwnerId= ap.[OwnerId]) SFUserActive, 

[Subject],

CASE 
WHEN ActivityTypeCode=4201 THEN 'Meeting'
WHEN ActivityTypeCode=4202 THEN 'Outbound Email'
ELSE NULL
END
AS
SFActivityType,

CASE 
WHEN ActivityTypeCode=4202 THEN 'Email'
WHEN ActivityTypeCode=4210 THEN 'Call'
WHEN ActivityTypeCode=4212 THEN 'Task'
else NULL
END
AS
SFActivitySubType,


CASE 
WHEN [RegardingObjectTypeCode] = 4 THEN cASt ([RegardingObjectId] AS VARCHAR(100))
WHEN [RegardingObjectTypeCode] = 2 THEN cASt ([RegardingObjectId] AS VARCHAR(100))
END  
AS CRM_Who_GUID__c,

CASE 
WHEN [RegardingObjectTypeCode] = 1  THEN [RegardingObjectId] 
WHEN [RegardingObjectTypeCode] = 3  THEN [RegardingObjectId] 
WHEN [RegardingObjectTypeCode] = 10027 THEN [RegardingObjectId]
WHEN [RegardingObjectTypeCode] = 4406 THEN [RegardingObjectId]

END AS CRM_What_GUID__c,

CASE 
-- Contact=2
WHEN [RegardingObjectTypeCode] = 2  THEN (select top 1 id from [dbo].[TempSFContact] where [CRM_Contact_ID__c]=[RegardingObjectId] and isdeleted=0 )
--Lead=4
WHEN [RegardingObjectTypeCode] = 4  THEN (select top 1 id from [dbo].[TempSFContact]   where [CRM_Lead_ID__c]=[RegardingObjectId] and isdeleted=0 )
END  AS SFWhoID,


CASE 
-- Acc=1
WHEN [RegardingObjectTypeCode] = 1  THEN (select top 1 id from [dbo].[TempSFAccount] where [CRM_ID__c]=[RegardingObjectId] and isdeleted=0 )
-- Opp=3
WHEN [RegardingObjectTypeCode] = 3  THEN (select top 1 id from [dbo].[TempSFOpp]  where [CRM_ID__c]=[RegardingObjectId] and isdeleted=0 )
--Reg=
WHEN [RegardingObjectTypeCode] = 10027  THEN (select tr.id from [dbo].[TempSFRegistration] tr join [ExecutiveEd_MSCRM].[dbo].[new_registrations] nr on nr.[new_enlightregistrationid] = tr.Registration_ID__c  where [new_registrationsId]=[RegardingObjectId] )
--4406 Quick Campaign
WHEN [RegardingObjectTypeCode] = 4406  THEN (select top 1 id from [dbo].[TempSFCampaign] where [CRM_ID__c]=[RegardingObjectId] and isdeleted=0 )

END  AS SFWhatId,


CASE 
-- Acc=1
WHEN [RegardingObjectTypeCode] = 1  THEN (select top 1 id from [dbo].[TempSFAccount] where [CRM_ID__c]=[RegardingObjectId] and isdeleted=0 )

END AS SFAccountId,


CASE 

WHEN ActivityTypeCode=4201 AND StateCode=1 AND StatusCode=3 THEN 'Completed'
WHEN ActivityTypeCode=4201 AND StateCode=3 AND StatusCode=5 THEN 'Not Started'

WHEN ActivityTypeCode=4202 AND StateCode=0 AND StatusCode=1 THEN 'In Progress'
WHEN ActivityTypeCode=4202 AND StateCode=1 AND StatusCode=3 THEN 'Sent'
WHEN ActivityTypeCode=4202 AND StateCode=1 AND StatusCode=4 THEN 'Received'
WHEN ActivityTypeCode=4202 AND StateCode=1 AND StatusCode=6 THEN 'Pending Send'

WHEN ActivityTypeCode=4208 AND StateCode=1 AND StatusCode=2 THEN 'Completed'

WHEN ActivityTypeCode=4210 AND StateCode=0 AND StatusCode=1 THEN 'In Progress'
WHEN ActivityTypeCode=4210 AND StateCode=1 AND StatusCode=2 THEN 'Completed'

WHEN ActivityTypeCode=4212 AND StateCode=0 AND StatusCode=2 THEN 'In Progress'
WHEN ActivityTypeCode=4212 AND StateCode=1 AND StatusCode=5 THEN 'Completed'

WHEN ActivityTypeCode=4401 AND StateCode=0 AND StatusCode=1 THEN 'In Progress'
WHEN ActivityTypeCode=4401 AND StateCode=1 AND StatusCode=2 THEN 'Completed'

WHEN ActivityTypeCode=4402 AND StateCode=0 AND StatusCode=6 THEN 'In Progress'
WHEN ActivityTypeCode=4406 AND StateCode=1 AND StatusCode=4 THEN 'Completed'

Else 'Not Started'
END
AS SFSTATUS,



CASE 
WHEN [PriorityCode] = 1 THEN 'Low'  
WHEN [PriorityCode] = 2 THEN 'Normal'  
WHEN [PriorityCode] = 3 THEN 'High'  
END AS SFPRIORITY,


[ModifiedOn],
[CreatedOn],
[ActualEND],
[ScheduledEND],
[ScheduledDurationMinutes],
[ActualDurationMinutes],  
[RegardingObjectIdName] AS SFRegarding__c,
[ActivityId] AS CRM_ID__c,
[ScheduledStart],
[Description],
[ActivityTypeCode],
[RegardingObjectTypeCode],
[RegardingObjectIdName],
[RegardingObjectId],
[StateCode],
[StatusCode],
[PriorityCode],
[InstanceTypeCode]


FROM 


dbo.FilteredActivityPointer ap























GO


