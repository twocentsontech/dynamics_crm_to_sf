---Make sure code, state and status number are correct in GetAllCRMActivitiesForSF.sql.
-- The current number in the script "GetAllCRMActivitiesForSF.sql" are for a patricular instance of CRM DB.  You may have different codes in your CRM DB
---Use the below queries

 
select 
distinct 
activitytypecode,
activitytypecodename, 
statecodename, statuscodename , statuscode, statecode
from dbo.FilteredActivityPointer

--ActivityTypeCode, Name
select 
distinct 
activitytypecode,
activitytypecodename 
from dbo.FilteredActivityPointer